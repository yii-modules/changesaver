<?php
/**
 * ChangeSaver 
 *
<code>
        // To save and resotre data
        <?php $this->widget('ext.ChangeSaver.ChangeSaver', array(
                'listenOnClass' => 'change-saver',
        )); ?>


        // To delete 
        Yii::app()->user->setState('_changeSaver_'.$listenOnClass, null); // $listenOnClass - as you set it in widget
</code>
 * 
 *
 * @version 1.0
 * @author vi mark <webvimark@gmail.com> 
 * @license MIT
 */
class ChangeSaver extends CWidget
{
        /**
         * listenOnClass 
         *
         * Class on your input field, which will be saved
         * 
         * @var string
         */
        public $listenOnClass;

        /**
         * init 
         */
        public function init()
        {
                if (! $this->listenOnClass) 
                        throw new CException("Set 'listenOnClass' parameter");

                $this->_saveData();

                Yii::app()->clientScript->registerScript('changeSaver_'.$this->listenOnClass,"
                        $(function(){
                                {$this->_restoreData()}
                                $('.{$this->listenOnClass}').on('change', function(){
                                        $.get('', { change_saver_name : $(this).attr('name'), change_saver_val : $(this).val() });
                                });
                        });
                ");

        }

        /**
         * _saveData 
         *
         * Get data from AJAX request and store it in session
         */
        private function _saveData()
        {
                if ( isset($_GET['change_saver_name']) AND isset($_GET['change_saver_val']) ) 
                {
                        $data = Yii::app()->user->getState('_changeSaver_'.$this->listenOnClass);

                        if (! $data) 
                                $data = array();

                        $data[$_GET['change_saver_name']] = $_GET['change_saver_val'];

                        Yii::app()->user->setState('_changeSaver_'.$this->listenOnClass, $data);
                }
        }

        /**
         * _restoreData 
         *
         * Read data in session and create script to restore it in fields
         * 
         * @return string
         */
        private function _restoreData()
        {
                $savedData = Yii::app()->user->getState('_changeSaver_'.$this->listenOnClass); 
                $restoreScript = '';

                if (is_array($savedData)) 
                {
                        foreach ($savedData as $key => $val) 
                                $restoreScript .= " $('[name=\"{$key}\"]').val(\"{$val}\");";
                }

                return $restoreScript;
        }
}